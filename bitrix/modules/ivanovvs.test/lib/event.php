<?php
namespace Ivanovvs\Test;
IncludeModuleLangFile(__FILE__);
class event
{
    /**
     * @param CAdminList &$list
     * @return void
     */
    public static function OnAdminListDisplayHandler(&$list)
    {
        $list->arActions["create_archive"] = "В архив";

    }

    //process create_archive
    function MyOnBeforeProlog()
    {
        // описанный в документации пример не работает. https://dev.1c-bitrix.ru/api_help/main/events/onadminlistdisplay.php

        if($_SERVER["REQUEST_METHOD"] == "POST" && $_POST["action"] == "create_archive" && is_array($_POST["ID"]) && $GLOBALS["APPLICATION"]->GetCurPage() == "/bitrix/admin/posting_admin.php")
        {
           print_r('Раздел помещен в архив');
        }
    }

}