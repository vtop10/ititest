<?
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

Class ivanovvs_test extends CModule{
	var	$MODULE_ID = 'ivanovvs.test';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;

	function __construct(){
		$arModuleVersion = array();
		include(__DIR__."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("IVANOVVS_TEST_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("IVANOVVS_TEST_MODULE_DESC");

		$this->PARTNER_NAME = getMessage("IVANOVVS_TEST_PARTNER_NAME");
		$this->PARTNER_URI = getMessage("IVANOVVS_TEST_PARTNER_URI");

		$this->exclusionAdminFiles=array(
			'..',
			'.',
			'menu.php',
			'operation_description.php',
			'task_description.php'
		);
	}

	function InstallDB($arParams = array()){
		$this->createNecessaryIblocks();
        $this->createNecessaryUserFields();
	}

	function UnInstallDB($arParams = array()){
		\Bitrix\Main\Config\Option::delete($this->MODULE_ID);
		$this->deleteNecessaryIblocks();
        $this->deleteNecessaryUserFields();
	}

	function InstallEvents(){
        \Bitrix\Main\EventManager::getInstance()->registerEventHandler('main', 'OnAdminListDisplay', $this->MODULE_ID, '\Ivanovvs\Test\Event', 'OnAdminListDisplayHandler');
        \Bitrix\Main\EventManager::getInstance()->registerEventHandler('main', 'OnBeforeProlog', $this->MODULE_ID, '\Ivanovvs\Test\Event', 'MyOnBeforeProlog');
		return true;
	}

	function UnInstallEvents(){
        \Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler('main', 'OnAdminListDisplay', $this->MODULE_ID, '\Ivanovvs\Test\Event', 'OnAdminListDisplayHandler');
        \Bitrix\Main\EventManager::getInstance()->unregisterEventHandler('main', 'OnBeforeProlog', $this->MODULE_ID, '\Ivanovvs\Test\Event', 'MyOnBeforeProlog');
		return true;
	}

	function InstallFiles($arParams = array()){
		$path = $this->GetPath()."/install/components";

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path)){
			CopyDirFiles($path, $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		}

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/admin')){
			CopyDirFiles($this->GetPath()."/install/admin/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin");
			if ($dir = opendir($path)){
				while (false !== $item = readdir($dir)){
					if (in_array($item, $this->exclusionAdminFiles))
						continue;
					file_put_contents($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$item,
						'<'.'? require($_SERVER["DOCUMENT_ROOT"]."'.$this->GetPath(true).'/admin/'.$item.'");?'.'>');
				}
				closedir($dir);
			}
		}

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/install/files')){
			$this->copyArbitraryFiles();
		}

		return true;
	}

	function UnInstallFiles(){
		\Bitrix\Main\IO\Directory::deleteDirectory($_SERVER["DOCUMENT_ROOT"].'/bitrix/components/'.$this->MODULE_ID.'/');

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/admin')){
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"].$this->GetPath().'/install/admin/', $_SERVER["DOCUMENT_ROOT"].'/bitrix/admin');
			if ($dir = opendir($path)){
				while (false !== $item = readdir($dir)){
					if (in_array($item, $this->exclusionAdminFiles))
						continue;
					\Bitrix\Main\IO\File::deleteFile($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.$this->MODULE_ID.'_'.$item);
				}
				closedir($dir);
			}
		}

		if (\Bitrix\Main\IO\Directory::isDirectoryExists($path = $this->GetPath().'/install/files')){
			$this->deleteArbitraryFiles();
		}

		return true;
	}

	function copyArbitraryFiles(){
		$rootPath = $_SERVER["DOCUMENT_ROOT"];
		$localPath = $this->GetPath().'/install/files';

		$dirIterator = new RecursiveDirectoryIterator($localPath, RecursiveDirectoryIterator::SKIP_DOTS);
		$iterator = new RecursiveIteratorIterator($dirIterator, RecursiveIteratorIterator::SELF_FIRST);

		foreach ($iterator as $object){
			$destPath = $rootPath.DIRECTORY_SEPARATOR.$iterator->getSubPathName();
			($object->isDir()) ? mkdir($destPath) : copy($object, $destPath);
		}
	}

	function deleteArbitraryFiles(){
		$rootPath = $_SERVER["DOCUMENT_ROOT"];
		$localPath = $this->GetPath().'/install/files';

		$dirIterator = new RecursiveDirectoryIterator($localPath, RecursiveDirectoryIterator::SKIP_DOTS);
		$iterator = new RecursiveIteratorIterator($dirIterator, RecursiveIteratorIterator::SELF_FIRST);

		foreach ($iterator as $object){
			if (!$object->isDir()){
				$file = str_replace($localPath, $rootPath, $object->getPathName());
				\Bitrix\Main\IO\File::deleteFile($file);
			}
		}
	}

	function createNecessaryIblocks(){
		$iblockType = $this->createIblockType();
		$iblock221ID = $this->createIblock(
			Array(
				"IBLOCK_TYPE_ID" => $iblockType,
				"ACTIVE" => "Y",
				"LID" => $this->getSitesIdsArray(),
				"VERSION" => "1",
				"CODE" => "modeli_avto",
				"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_NAME"),
				"SORT" => "500",
				"LIST_PAGE_URL" => "#SITE_DIR#/test/index.php?ID=#IBLOCK_ID#",
				"SECTION_PAGE_URL" => "#SITE_DIR#/test/list.php?SECTION_ID=#SECTION_ID#",
				"DETAIL_PAGE_URL" => "#SITE_DIR#/test/detail.php?ID=#ELEMENT_ID#",
				"INDEX_SECTION" => "Y",
				"INDEX_ELEMENT" => "Y",
				"FIELDS" => Array(
					"ACTIVE" => Array(
						"DEFAULT_VALUE" => "Y",
					),
					"PREVIEW_TEXT_TYPE" => Array(
						"DEFAULT_VALUE" => "text",
					),
					"PREVIEW_TEXT_TYPE_ALLOW_CHANGE" => Array(
						"DEFAULT_VALUE" => "N",
					),
					"DETAIL_TEXT_TYPE" => Array(
						"DEFAULT_VALUE" => "text",
					),
					"DETAIL_TEXT_TYPE_ALLOW_CHANGE" => Array(
						"DEFAULT_VALUE" => "N",
					),
				),
				"GROUP_ID" => Array('2' => 'R'),
			)
		);
		$prop1094ID = $this->createIblockProp(
			Array(
				"IBLOCK_ID" => $iblock221ID,
				"ACTIVE" => "Y",
				"SORT" => "500",
				"CODE" => "MARKA",
				"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_PARAM_1094_NAME"),
				"PROPERTY_TYPE" => "S",
				"USER_TYPE" => "",
				"MULTIPLE" => "N",
				"IS_REQUIRED" => "N",
			)
		);
		$prop1095ID = $this->createIblockProp(
			Array(
				"IBLOCK_ID" => $iblock221ID,
				"ACTIVE" => "Y",
				"SORT" => "500",
				"CODE" => "MODEL",
				"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_PARAM_1095_NAME"),
				"PROPERTY_TYPE" => "S",
				"USER_TYPE" => "",
				"MULTIPLE" => "N",
				"IS_REQUIRED" => "N",
			)
		);
		$prop1096ID = $this->createIblockProp(
			Array(
				"IBLOCK_ID" => $iblock221ID,
				"ACTIVE" => "Y",
				"SORT" => "500",
				"CODE" => "FOTO",
				"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_PARAM_1096_NAME"),
				"PROPERTY_TYPE" => "F",
				"USER_TYPE" => "",
				"MULTIPLE" => "N",
				"IS_REQUIRED" => "N",
				"HINT" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_PARAM_1096_HINT"),
				"FILE_TYPE" => "jpg,jpeg,png,png,gif",
			)
		);
		$section26ID = $this->createIblockSection(
			Array(
				"IBLOCK_ID" => $iblock221ID,
				"ACTIVE" => "Y",
				"SORT" => "500",
				"CODE" => "sovremennye_avtomobili",
				"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_SECTION_26_NAME"),
			)
		);
$this->createIblockElement(
	Array(
		"IBLOCK_ID" => $iblock221ID,
		"ACTIVE" => "Y",
		"SORT" => "500",
		"CODE" => "honda_accord",
		"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_464_NAME"),
		"IBLOCK_SECTION_ID" => $section26ID,
				"PROPERTY_VALUES" => Array(
					$prop1094ID => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_464_PROP_1094_VALUE"),
					$prop1095ID => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_464_PROP_1095_VALUE"),
				),
	)
);
$this->createIblockElement(
	Array(
		"IBLOCK_ID" => $iblock221ID,
		"ACTIVE" => "Y",
		"SORT" => "500",
		"CODE" => "honda_torneo",
		"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_465_NAME"),
		"IBLOCK_SECTION_ID" => $section26ID,
				"PROPERTY_VALUES" => Array(
					$prop1094ID => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_465_PROP_1094_VALUE"),
					$prop1095ID => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_465_PROP_1095_VALUE"),
				),
	)
);
$this->createIblockElement(
	Array(
		"IBLOCK_ID" => $iblock221ID,
		"ACTIVE" => "Y",
		"SORT" => "500",
		"CODE" => "Legend",
		"NAME" => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_466_NAME"),
		"IBLOCK_SECTION_ID" => $section26ID,
				"PROPERTY_VALUES" => Array(
					$prop1094ID => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_466_PROP_1094_VALUE"),
					$prop1095ID => Loc::getMessage("IVANOVVS_TEST_IBLOCK_MODELI_AVTO_ELEMENT_466_PROP_1095_VALUE"),
				),
	)
);
	}

	function deleteNecessaryIblocks(){
		$this->removeIblockType();
	}

	function createNecessaryUserFields(){
		return true;
	}

	function deleteNecessaryUserFields(){
		return true;
	}

	function createNecessaryMailEvents(){
		return true;
	}

	function deleteNecessaryMailEvents(){
		return true;
	}

	function isVersionD7(){
		return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
	}

	function GetPath($notDocumentRoot = false){
		if ($notDocumentRoot){
			return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
		}else{
			return dirname(__DIR__);
		}
	}

	function getSitesIdsArray(){
		$ids = Array();
		$rsSites = CSite::GetList($by = "sort", $order = "desc");
		while ($arSite = $rsSites->Fetch()){
			$ids[] = $arSite["LID"];
		}

		return $ids;
	}

	function createIblockType(){
	global $DB, $APPLICATION;
	CModule::IncludeModule("iblock");

	$iblockType = "ivanovvs_test_iblock_type";
	$db_iblock_type = CIBlockType::GetList(Array("SORT" => "ASC"), Array("ID" => $iblockType));
	if (!$ar_iblock_type = $db_iblock_type->Fetch()){
		$arFieldsIBT = Array(
			'ID'       => $iblockType,
			'SECTIONS' => 'Y',
			'IN_RSS'   => 'N',
			'SORT'     => 500,
			'LANG'     => Array(
				'en' => Array(
					'NAME' => Loc::getMessage("IVANOVVS_TEST_IBLOCK_TYPE_NAME_EN"),
				),
				'ru' => Array(
					'NAME' => Loc::getMessage("IVANOVVS_TEST_IBLOCK_TYPE_NAME_RU"),
				)
			)
		);

		$obBlocktype = new CIBlockType;
		$DB->StartTransaction();
		$resIBT = $obBlocktype->Add($arFieldsIBT);
		if (!$resIBT){
			$DB->Rollback();
			$APPLICATION->ThrowException(Loc::getMessage("IVANOVVS_TEST_IBLOCK_TYPE_ALREADY_EXISTS"));
		}else{
			$DB->Commit();

			return $iblockType;
		}
	}
}

function removeIblockType(){
	global $APPLICATION, $DB;
	CModule::IncludeModule("iblock");

	$iblockType = "ivanovvs_test_iblock_type";

	$DB->StartTransaction();
	if (!CIBlockType::Delete($iblockType)){
		$DB->Rollback();
		$APPLICATION->ThrowException(Loc::getMessage("IVANOVVS_TEST_IBLOCK_TYPE_DELETION_ERROR"));
	}
	$DB->Commit();
}

function createIblock($params){
	global $APPLICATION;
	CModule::IncludeModule("iblock");

	$ib = new CIBlock;

	$resIBE = CIBlock::GetList(Array(), Array('TYPE' => $params["IBLOCK_TYPE_ID"], 'SITE_ID' => $params["SITE_ID"], "CODE" => $params["CODE"]));
	if ($ar_resIBE = $resIBE->Fetch()){
		$APPLICATION->ThrowException(Loc::getMessage("IVANOVVS_TEST_IBLOCK_ALREADY_EXISTS"));

		return false;
	}else{
		$ID = $ib->Add($params);

		return $ID;
	}

	return false;
}

function createIblockProp($arFieldsProp){
	CModule::IncludeModule("iblock");
	$ibp = new CIBlockProperty;

	return $ibp->Add($arFieldsProp);
}



function createIblockSection($arFields){
	CModule::IncludeModule("iblock");
	$el = new CIBlockSection;

	if ($SECTION_ID = $el->Add($arFields)){
		return $SECTION_ID;
	}

	return false;
}



function createIblockElement($arFields){
	CModule::IncludeModule("iblock");
	$el = new CIBlockElement;

	if ($PRODUCT_ID = $el->Add($arFields)){
		return $PRODUCT_ID;
	}

	return false;
}

function DoInstall(){

		global $APPLICATION;
		if ($this->isVersionD7()){
			\Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);

			$this->InstallDB();
			$this->createNecessaryMailEvents();
			$this->InstallEvents();
			$this->InstallFiles();
		}else{
			$APPLICATION->ThrowException(Loc::getMessage("IVANOVVS_TEST_INSTALL_ERROR_VERSION"));
		}

		$APPLICATION->IncludeAdminFile(Loc::getMessage("IVANOVVS_TEST_INSTALL"), $this->GetPath()."/install/step.php");
	}

	function DoUninstall(){

		global $APPLICATION;

		$context = Application::getInstance()->getContext();
		$request = $context->getRequest();

		$this->UnInstallFiles();
		$this->deleteNecessaryMailEvents();
		$this->UnInstallEvents();

		if ($request["savedata"] != "Y")
			$this->UnInstallDB();

		\Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

		$APPLICATION->IncludeAdminFile(Loc::getMessage("IVANOVVS_TEST_UNINSTALL"), $this->GetPath()."/install/unstep.php");
	}
}
?>