<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("IVANOVVS_VYVOD_VREMENI_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("IVANOVVS_VYVOD_VREMENI_COMPONENT_DESCRIPTION"),
	"ICON" => "/images/regions.gif",
	"SORT" => 500,
	"PATH" => array(
		"ID" => "ivanovvs_test_components",
		"SORT" => 500,
		"NAME" => GetMessage("IVANOVVS_VYVOD_VREMENI_COMPONENTS_FOLDER_NAME"),
	),
);

?>