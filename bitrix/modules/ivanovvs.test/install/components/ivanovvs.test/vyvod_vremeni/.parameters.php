<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
    'GROUPS' => array(
        'SOCIAL' => array(
            'SORT' => 110,
            'NAME' => GetMessage('SOCIAL'),
        )
    ),
    'PARAMETERS' => array(
        'TIME_FORMAT' => array(
            'NAME' => 'Формат вывода времени',
            'TYPE' => 'LIST',
            'VALUES' => array(
                '24' => '24H',
                '12' => '12H',

            ),
            'DEFAULT' => '24H',
        ),
        'TIMEZONE' => array(
            'NAME' => 'Часовой пояс',
            'TYPE' => 'LIST',
            'VALUES' => array(
                '-1' => 'Калининградское',
                '0' => 'Московское',
                '1' => 'Самарское',

            ),
            'DEFAULT' => '24H',
        ),
    )
);
?>
